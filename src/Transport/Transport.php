<?php
namespace Transport;

interface Transport
{
    public function setHost($host);
    public function connect();
    public function disconnect();
    public function read();
    public function write($data);
}
