<?php
namespace Transport;

use Net\Ipv4;
use Net\Ipv6;

class Socket implements Transport
{
    /**
     * Транспортный протокол
     * 
     * @var boolean по-умолчанию будет использован UDP
     */
    public $is_udp = true;
    
    /**
     * Хост
     *
     * @var string
     */
    public $host;
    
    /**
     * Порт
     * 
     * @var int
     */
    public $port = 53;
    
    /**
     * Таймаут соединения, секунд
     * 
     * @var int
     */
    public $connection_timeout = 5;
    
    /**
     * Таймаут операций чтения/записи, секунд
     * 
     * @var int
     */
    public $stream_timeout = 5;
    
    /**
     * Код ошибки подключения
     * 
     * @var int
     */
    public $errno;
    
    /**
     * Описание ошибки подключения
     * 
     * @var string
     */
    public $errstr;
    
    /**
     * Ресурс сокета
     * 
     * @var resource
     */
    protected $socket;
    
    /**
     * Устанавливает DNS-сервер для поключения
     *
     * @param string $host
     * @throws Exception
     */
    public function setHost($host)
    {
        if (!Ipv4::isValid($host) && !Ipv6::isValid($host)) {
            throw new Exception("'$host' не является валидным IP");
        }
    
        $this->host = $host;
    }
    
    /**
     * Инициирует соединение с хостом
     */
    public function connect()
    {
        $this->socket = fsockopen(
            $this->is_udp ? "udp://{$this->host}" : "tcp://{$this->host}",
            $this->port,
            $this->errno,
            $this->errstr,
            $this->connection_timeout
        );
        
        if (!$this->socket) {
            throw new Exception(
                "Ошибка подключения к '{$this->host}': {$this->errstr}"
            );
        }
        
        stream_set_timeout($this->socket, $this->stream_timeout);
    }
    
    /**
     * Закрывает соединение с хостом
     */
    public function disconnect()
    {
        fclose($this->socket);
    }
    
    /**
     * Передает данные хосту
     * 
     * @param string $data
     */
    public function write($data)
    {
        $size = strlen($data);
        
        if (!$this->is_udp) {
            fwrite($this->socket, pack('C', $size));
        }
        
        fwrite($this->socket, $data, $size);
    }
    
    /**
     * Получает данные с хоста
     * 
     * @return string
     */
    public function read()
    {
        $response = '';
        
        if ($this->is_udp) {
            $response = fread($this->socket, 1024);
        } else {
            $response_sizebin = fread($this->socket, 2);
            $response_info = unpack('nsize', $response_sizebin);
            $response = fread($this->socket, $response_info['size']);
        }
        
        return $response;
    }
}
