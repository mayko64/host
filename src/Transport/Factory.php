<?php
namespace Transport;

abstract class Factory
{
    public static function create($transport)
    {
        switch ($transport) {
            case 'socket':
                return new Socket();
                break;
            default:
                throw new Exception("Транспорт '$transport' не реализован");
                break;
        }
    }
}
