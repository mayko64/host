<?php
namespace Dns;

/**
 * Классы RR
 * 
 * @see RFC1035, 3.2.4
 */
abstract class Qclass
{
    const IN = 1;
    // ...
    
    protected static $valueByClass = array(
        'IN' => self::IN,
        // ...
    );
    
    protected static $classByValue;
    
    /**
     * Возвращает числовое значение поля QCLASS
     * 
     * @param string $class
     * @return int
     */
    public static function getValue($class)
    {
        if (!array_key_exists($class, self::$valueByClass)) {
            return $class;
        }
        
        return self::$valueByClass[$class];
    }
    
    /**
     * Возвращает ID поля QCLASS по его числовому значению
     * 
     * @param int $value
     * @return string
     */
    public static function getClass($value)
    {
        if (!self::$classByValue) {
            self::$classByValue = array_flip(self::$valueByClass);
        }
        
        if (!array_key_exists($value, self::$classByValue)) {
            return $value;
        }
        
        return self::$classByValue[$value];
    }
}
