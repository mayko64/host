<?php
namespace Dns;

/**
 * Resource Record (RR)
 * 
 *                                  1  1  1  1  1  1
 *    0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |                                               |
 *  /                                               /
 *  /                      NAME                     /
 *  |                                               |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |                      TYPE                     |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |                     CLASS                     |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |                      TTL                      |
 *  |                                               |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |                   RDLENGTH                    |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
 *  /                     RDATA                     /
 *  /                                               /
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  
 *  @see RFC1035, 4.1.3
 */
class RR
{
    /**
     * @var string
     */
    public $name;
    
    /**
     * @var int
     */
    public $type;
    
    /**
     * @var int
     */
    public $class;
    
    /**
     * @var int
     */
    public $ttl;
    
    /**
     * @var int
     */
    public $rdlength;
    
    /**
     * @var string
     */
    public $rdata;
    
    public function __toString()
    {
        $buff = '';
        $buff .= $this->name . "\t";
        $buff .= Qclass::getClass($this->class) . "\t";
        $buff .= Qtype::getType($this->type) . "\t";
        $buff .= $this->ttl . "\t";
        $buff .= $this->rdata . PHP_EOL;
        
        return $buff;
    }
}
