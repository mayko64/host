<?php
namespace Dns;

use Net\Ipv4;
use Net\Ipv6;
use Net\Domain;
use Net\Payload;

/**
 * DNS-клиент
 */
class Client
{
    /**
     * Транспорты для обмена данными с сервером
     * 
     * @var \Transport\Transport[]
     */
    protected $transports = array();
    
    /**
     * Добавляет транспорт
     * 
     * @param \Transport\Transport $transport
     */
    public function addTransport(\Transport\Transport $transport)
    {
        $this->transports[] = $transport;
    }
    
    /**
     * Добавляет список транспортов
     * 
     * @param array $transports
     */
    public function addTransports(array $transports)
    {
        foreach ($transports as $transport) {
            $this->addTransport($transport);
        }
    }
    
    /**
     * Выполняет запрос
     * 
     * @param string $arg IP-адрес или доменное имя
     * @return array
     */
    public function query($arg)
    {
        $packets = array();
        $responses = array();
        
        $payload = $this->getPayload($arg);
        
        $packet = new Packet();
        $packet->setPayload($payload);
        $packets[] = $packet;
        
        // для доменов отдельным запросом получим записи AAAA
        if ($payload instanceof Domain) {
            $packet = new Packet();
            $packet->setPayload($payload);
            $packet->question->qtype = Qtype::AAAA;
            $packets[] = $packet;
        }
        
        $transport_ptr = 0;
        
        do {
            if (!array_key_exists($transport_ptr, $this->transports)) {
                throw new Exception("Больше нет серверов для подключения");
            }
            
            $transport = $this->transports[$transport_ptr];
            
            try {
                $transport->connect();
            
                foreach ($packets as $packet) {
                    $transport->write($packet->toBinary());
                    $bindata = $transport->read();
                    $response = new Packet();
                    $response->fromString($bindata);
                    $responses[] = $response;
                }
                
                $transport->disconnect();
            } catch (\Exception $e) {
                // проблемы с соединением, битый пакет и пр. - 
                // работаем со следующим сервером
                $transport_ptr++;
                continue;
            }
            
            break;
        } while ($transport_ptr < count($this->transports));
        
        return $responses;
    }
    
    /**
     * Создает аргумент для формирования пакета
     * 
     * @param string $arg
     * @return Payload
     */
    protected function getPayload($arg)
    {
        $payload = null;
        
        if (Ipv4::isValid($arg)) {
            $payload = new Ipv4($arg);
        } elseif (Ipv6::isValid($arg)) {
            $payload = new Ipv6($arg);
        } else {
            $payload = new Domain($arg);
        }
        
        return $payload;
    }
}
