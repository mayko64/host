<?php
namespace Dns;

/**
 * Типы RR
 * 
 * @see RFC1035, 3.2.2
 * @see RFC3596, 2.1
 */
abstract class Qtype
{
    const A = 1;
    const NS = 2;
    const CNAME = 5;
    const SOA = 6;
    const PTR = 12;
    const AAAA = 28;
    // ...
    
    protected static $valueByType = array(
        'A' => self::A,
        'NS' => self::NS,
        'CNAME' => self::CNAME,
        'SOA' => self::SOA,
        'PTR' => self::PTR,
        'AAAA' => self::AAAA,
        // ...
    );
    
    protected static $typeByValue;
    
    /**
     * Возвращает числовое значение поля QTYPE
     * 
     * @param string $type
     * @return int
     */
    public static function getValue($type)
    {
        if (!array_key_exists($type, self::$valueByType)) {
            return $type;
        }
        
        return self::$valueByType[$type];
    }
    
    /**
     * Возвращает ID поля QTYPE по его числовому значению
     * 
     * @param int $value
     * @return string
     */
    public static function getType($value)
    {
        if (!self::$typeByValue) {
            self::$typeByValue = array_flip(self::$valueByType);
        }
        
        if (!array_key_exists($value, self::$typeByValue)) {
            return $value;
        }
        
        return self::$typeByValue[$value];
    }
}
