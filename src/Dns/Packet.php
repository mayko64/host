<?php
namespace Dns;

use Dns\Packet\Header;
use Dns\Packet\Question;
use Dns\Packet\Answer;
use Dns\Packet\Authority;
use Dns\Packet\Additional;
use Net\Payload;

/**
 * Пакет данных
 * 
 *  +---------------------+
 *  |        Header       |
 *  +---------------------+
 *  |       Question      | the question for the name server
 *  +---------------------+
 *  |        Answer       | RRs answering the question
 *  +---------------------+
 *  |      Authority      | RRs pointing toward an authority
 *  +---------------------+
 *  |      Additional     | RRs holding additional information
 *  +---------------------+
 *
 * @see RFC1035, 4.1
 */
class Packet
{
    /**
     * @var Packet\Header
     */
    public $header;
    
    /**
     * @var Packet\Question
     */
    public $question;
    
    /**
     * @var Packet\Answer
     */
    public $answer;
    
    /**
     * @var Packet\Authority
     */
    public $authority;
    
    /**
     * @var Packet\Additional
     */
    public $additional;
    
    /**
     * Текущая позиция
     * 
     * Используется при разборе полученного бинарного ответа
     * 
     * @var int
     */
    protected $offset;
    
    /**
     * Бинарное представление
     * 
     * @var string
     */
    protected $bindata;
    
    public function __construct()
    {
        $this->header = new Header();
        $this->question = new Question();
        $this->answer = new Answer();
        $this->authority = new Authority();
        $this->additional = new Additional();
    }
    
    /**
     * Заполняет поля перед отправкой
     * 
     * @param \Net\Payload $payload
     */
    public function setPayload(Payload $payload)
    {
        $this->question->setPayload($payload);
    }
    
    /**
     * Парсит бинарный ответ и заполняет свойства объекта
     * 
     * @param string $bindata
     */
    public function fromString($bindata)
    {
        $this->bindata = $bindata;
        $this->offset = 0;
        
        $this->header->fromPacket($this);
        
        if ($this->header->qr == Header::QR_QUERY) {
            throw new Exception("Получен пакет типа QUERY");
        }
        
        if ($this->header->tc == 1) {
            throw new Exception("Невозможно разобрать пакет: truncation detected");
        }
        
        switch ($this->header->rcode) {
            case Header::RCODE_FORMAT_ERROR:
                throw new Exception("Format Error");
                break;
            case Header::RCODE_SERVER_ERROR:
                throw new Exception("Server Error");
                break;
            case Header::RCODE_NAME_ERROR:
                throw new Exception("Name Error");
                break;
            case Header::RCODE_NOT_IMPLEMENTED:
                throw new Exception("Not Implemented");
                break;
            case Header::RCODE_REFUSED:
                throw new Exception("Refused");
                break;
            case Header::RCODE_OK:
                break;
            default:
                throw new Exception("Некорректное значение RCODE '{$this->header->rcode}'");
                break;
        }
        
        $this->question->fromPacket($this);
        $this->answer->fromPacket($this);
        $this->authority->fromPacket($this);
        $this->additional->fromPacket($this);
    }
    
    /**
     * Читает бинарные данные в полученном ответе
     * 
     * Возвращает $bytes байт начиная со сдвига $offset
     * 
     * @param int $bytes
     * @param int $offset
     * @return string
     */
    public function read($bytes = 1, &$offset = '')
    {
        if ($offset == '')
            $offset =& $this->offset;

        $s = substr($this->bindata, $offset, $bytes);
        $offset += $bytes;

        return $s;
    }
    
    /**
     * Разбирает метки (labels) в бинарной строке
     * 
     * @param string $offset
     * @return array
     */
    public function readLabels(&$offset = '')
    {
        if ($offset == '')
            $offset =& $this->offset;
    
        $labels = array();
    
        do {
            $char1 = $this->read(1, $offset);
            $len = ord($char1);
    
            if ($len < 1) { // конец данных
                break;
            }
    
            if ($len < 64) { // несжатые данные
                $labels = array_merge($labels, explode('.', $this->read($len, $offset)));
            }
    
            if ($len >= 64) { // сжатые данные
                $char2 = $this->read(1, $offset);
                $pointer_offset = (($len & 0x3f) << 8) + ord($char2);
                $labels = array_merge($labels, $this->readLabels($pointer_offset));
                break;
            }
        }
        while (true);
    
        return $labels;
    }
    
    /**
     * Читает RR-запись
     * 
     * @return RR
     */
    public function readRR()
    {
        $labels = $this->readLabels();

        $rr = new RR();
        $rr->name = implode('.', $labels);

        $data = $this->read(10);
        $info = unpack('ntype/nclass/Nttl/nrdlength', $data);

        $rr->type = $info['type'];
        $rr->class = $info['class'];
        $rr->ttl = $info['ttl'];
        $rr->rdlength = $info['rdlength'];

        switch ($rr->type) {
            case Qtype::A:
            case Qtype::AAAA:
                $rr->rdata = inet_ntop($this->read($info['rdlength']));
                break;
            case Qtype::CNAME:
            case Qtype::PTR:
            case Qtype::NS:
                $rr->rdata = implode('.', $this->readLabels());
                break;
            default:
                $rr->rdata = "Неподдерживаемый тип '{$rr->type}'";
                break;
        }

        return $rr;
    }
    
    /**
     * Подготавливает пакет к отправке
     */
    public function compile()
    {
        $this->bindata = $this->header->toBinary() . $this->question->toBinary();
    }
    
    public function toBinary()
    {
        if (!$this->bindata) {
            $this->compile();
        }
        
        return $this->bindata;
    }
    
    public function __toString()
    {
        return "; HEADER" . PHP_EOL . (string)$this->header . PHP_EOL .
            "; QUESTION" . PHP_EOL . (string)$this->question . PHP_EOL .
            "; ANSWER" . PHP_EOL . (string)$this->answer . PHP_EOL .
            "; AUTHORITY" . PHP_EOL . (string)$this->authority . PHP_EOL .
            "; ADDITIONAL" . PHP_EOL . (string)$this->additional;
    }
}
