<?php
namespace Dns\Packet;

use Dns\Qtype;
use Dns\Qclass;
use Net\Ipv4;
use Net\Ipv6;
use Net\Domain;
use Net\Payload;
use Dns\Exception;
use Dns\Packet;

/**
 * Секция QUESTION пакета
 * 
 *                                 1  1  1  1  1  1
 *   0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * |                                               |
 * /                     QNAME                     /
 * /                                               /
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * |                     QTYPE                     |
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * |                     QCLASS                    |
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  
 * @see RFC1035, 4.1.2
 */
class Question
{
    /**
     * Секция QNAME
     * 
     * Представлена массивом меток (labels)
     * 
     * @var array
     */
    public $qname;
    
    /**
     * @var int
     */
    public $qtype;
    
    /**
     * @var int
     */
    public $qclass;
    
    /**
     * Бинарные данные
     * 
     * @var string
     */
    protected $bindata;
    
    /**
     * Заполняет поля секции Question перед отправкой
     * 
     * @param \Net\Payload $payload
     * @throws Exception
     */
    public function setPayload(Payload $payload)
    {
        $this->qclass = Qclass::IN;
        $this->qname = array();
        
        if ($payload instanceof Ipv4) {
            $this->qname = array_reverse(explode('.', $payload->value));
            $this->qname[] = 'IN-ADDR';
            $this->qname[] = 'ARPA';
            $this->qtype = Qtype::PTR;
        } elseif ($payload instanceof Ipv6) {
            $this->qname = array_reverse(str_split(str_replace(
                ':',
                '',
                $payload->getExpanded()
            )));
            $this->qname[] = 'IP6';
            $this->qname[] = 'ARPA';
            $this->qtype = Qtype::PTR;
        } elseif ($payload instanceof Domain) {
            $this->qname = explode('.', $payload->value);
            $this->qtype = Qtype::A;
        } else {
            throw new Exception(
                "Неподдерживаемый класс параметра '" . get_class($payload) . "'"
            );
        }
    }
    
    /**
     * Парсит бинарный ответ и заполняет свойства объекта
     * 
     * @param Packet $packet
     */
    public function fromPacket(Packet &$packet)
    {
        $this->qname = $packet->readLabels();
        
        $bindata = $packet->read(4);
        $info = unpack('nqtype/nqclass', $bindata);
        $this->qtype = $info['qtype'];
        $this->qclass = $info['qclass'];
    }
    
    /**
     * Собирает бинарную строку секции
     */
    protected function compile()
    {
        $this->bindata = '';
        
        foreach ($this->qname as $label) {
            $this->bindata .= pack('C', strlen($label));
            $this->bindata .= $label;
        }
        
        $this->bindata .= pack('C', 0);
        $this->bindata .= pack('n', $this->qtype);
        $this->bindata .= pack('n', $this->qclass);
    }
    
    public function toBinary()
    {
        if (!$this->bindata) {
            $this->compile();
        }
        
        return $this->bindata;
    }
    
    public function __toString()
    {
        $buff = '';
        $buff .= implode('.', $this->qname) . "\t";
        $buff .= Qclass::getClass($this->qclass) . "\t";
        $buff .= Qtype::getType($this->qtype) . PHP_EOL;
        
        return $buff;
    }
}
