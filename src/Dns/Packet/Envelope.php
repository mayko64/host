<?php
namespace Dns\Packet;

use Dns\RR;
use Dns\Packet;

/**
 * Пакет RR
 * 
 * Представляет собой секции ANSWER, AUTHORITY и ADDITIONAL
 */
abstract class Envelope
{
    /**
     * @var array
     */
    protected $records = array();

    /**
     * Добавляет RR
     *
     * @param RR $rr
     */
    public function add(RR $rr)
    {
        $this->records[] = $rr;
    }

    public function __toString()
    {
        $buff = '';

        foreach ($this->records as $rr) {
            $buff .= (string)$rr;
        }

        return $buff;
    }

    /**
     * Парсит бинарный ответ и заполняет свойства объекта
     * 
     * Реализация чуть разная для ANSWER, AUTHORITY и ADDITIONAL
     * 
     * @param Packet $packet
     */
    abstract public function fromPacket(Packet &$packet);
}
