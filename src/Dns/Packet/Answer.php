<?php
namespace Dns\Packet;

use Dns\Packet;

/**
 * Секция ANSWER
 *
 * @see RFC1035, 4.1, 4.1.3
 */
class Answer extends Envelope
{
    public function fromPacket(Packet &$packet)
    {
        for ($i = 0; $i < $packet->header->ancount; $i++) {
            $this->add($packet->readRR());
        }
    }
}
