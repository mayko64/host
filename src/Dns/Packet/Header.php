<?php
namespace Dns\Packet;

use Dns\Packet;

/**
 * Секция HEADER пакета
 *
 *                                  1  1  1  1  1  1
 *    0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |                      ID                       |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |                    QDCOUNT                    |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |                    ANCOUNT                    |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |                    NSCOUNT                    |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  |                    ARCOUNT                    |
 *  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  
 * @see RFC1035, 4.1.1
 */
class Header
{
    const QR_QUERY = 0;
    const QR_RESPONSE = 1;
    
    const OPCODE_QUERY = 0;
    const OPCODE_IQUERY = 1;
    const OPCODE_STATUS = 2;
    
    const RCODE_OK = 0;
    const RCODE_FORMAT_ERROR = 1;
    const RCODE_SERVER_ERROR = 2;
    const RCODE_NAME_ERROR = 3;
    const RCODE_NOT_IMPLEMENTED = 4;
    const RCODE_REFUSED = 5;
    
    /**
     * @var int
     */
    public $id;
    
    /**
     * @var int
     */
    public $qr = self::QR_QUERY;
    
    /**
     * @var int
     */
    public $opcode = self::OPCODE_QUERY;
    
    /**
     * @var int
     */
    public $aa = 0;
    
    /**
     * @var int
     */
    public $tc = 0;
    
    /**
     * @var int
     */
    public $rd = 0x0100;
    
    /**
     * @var int
     */
    public $ra = 0;
    
    /**
     * @var int
     */
    public $z = 0;
    
    /**
     * @var int
     */
    public $rcode;
    
    /**
     * @var int
     */
    public $qdcount = 1;
    
    /**
     * @var int
     */
    public $ancount;
    
    /**
     * @var int
     */
    public $nscount;
    
    /**
     * @var int
     */
    public $arcount;
    
    /**
     * Бинарные данные
     * 
     * @var string
     */
    protected $bindata;
    
    /**
     * Парсит бинарный ответ и заполняет свойства объекта
     * 
     * @param \Dns\Packet $packet
     */
    public function fromPacket(Packet &$packet)
    {
        $binheader = $packet->read(12);
        $header = unpack("nid/nflags/nqdcount/nancount/nnscount/narcount", $binheader);
        $this->id = $header['id'];
        $this->rcode = $header['flags'] & 15;
        $this->z = ($header['flags'] >> 4) & 7;
        $this->ra = ($header['flags'] >> 7) & 1;
        $this->rd = ($header['flags'] >> 8) & 1;
        $this->tc = ($header['flags'] >> 9) & 1;
        $this->aa = ($header['flags'] >> 10) & 1;
        $this->opcode = ($header['flags'] >> 11) & 15;
        $this->qr = ($header['flags'] >> 15) & 1;
        $this->qdcount = $header['qdcount'];
        $this->ancount = $header['ancount'];
        $this->nscount = $header['nscount'];
        $this->arcount = $header['arcount'];
    }
    
    protected function setRandomId()
    {
        $this->id = rand(1, 255);
    }
    
    /**
     * Собирает бинарную строку секции
     */
    protected function compile()
    {
        $this->setRandomId();
        
        $this->bindata = '';
        $this->bindata .= pack('n', $this->id);
        $this->bindata .= pack('n',
            $this->qr | $this->opcode | $this->aa | $this->tc | 
            $this->rd | $this->ra | $this->z | $this->rcode
        );
        $this->bindata .= pack('nn', $this->qdcount, $this->ancount);
        $this->bindata .= pack('nn', $this->nscount, $this->arcount);
    }
    
    public function toBinary()
    {
        if (!$this->bindata) {
            $this->compile();
        }
        
        return $this->bindata;
    }
    
    public function __toString()
    {
        $buff = '';
        $buff .= "id: {$this->id}, ";
        $buff .= "opcode: {$this->opcode}, ";
        $buff .= "rcode: {$this->rcode}" . PHP_EOL;
        
        $flags = array();
        if ($this->aa)
            $flags[] = 'aa';
        if ($this->tc)
            $flags[]= 'tc';
        if ($this->rd)
            $flags[] = 'rd';
        if ($this->ra)
            $flags[] = 'ra';
        
        $buff .= "flags: " . (empty($flags) ? 'none' : implode(', ', $flags));
        $buff .= PHP_EOL;
        
        $buff .= "answers: {$this->ancount}, ";
        $buff .= "authorities: {$this->nscount}, ";
        $buff .= "additional: {$this->arcount}" . PHP_EOL;
        
        return $buff;
    }
}

