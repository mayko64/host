<?php
namespace Dns\Packet;

use Dns\Packet;

/**
 * Секция ADDITIONAL
 *
 * @see RFC1035, 4.1, 4.1.3
 */
class Authority extends Envelope
{
    public function fromPacket(Packet &$packet)
    {
        for ($i = 0; $i < $packet->header->nscount; $i++) {
            $this->add($packet->readRR());
        }
    }
}
