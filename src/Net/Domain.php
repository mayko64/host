<?php
namespace Net;

class Domain extends Payload
{
    public function __construct($domain)
    {
        $this->value = $domain;
    }
    
    /**
     * Проверяет, валиден ли $domain
     * 
     * @param string $domain
     */
    public static function isValid($domain)
    {
        // проверка достаточно условна
        return (is_string($domain) and strpos($domain, '.') !== false);
    }
}
