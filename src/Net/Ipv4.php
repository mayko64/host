<?php
namespace Net;

/**
 * IPv4-адрес
 */
class Ipv4 extends Ip
{
    /**
     * Упакованный адрес
     *
     * @var int
     */
    protected $in_addr;
    
    public function __construct($ip)
    {
        if (!self::isValid($ip)) {
            throw new Exception("'$ip' не является валидным IPv6-адресом");
        }
        
        $this->value = $ip;
    }
    
    /**
     * Проверяет, является ли $ip валидным IPv4-адресом
     *
     * @param string $ip
     * @return boolean
     */
    public static function isValid($ip)
    {
        return (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) !== false);
    }
}
