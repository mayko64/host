<?php
namespace Net;

/**
 * IPv6-адрес
 */
class Ipv6 extends Ip
{
    /**
     * Адрес в упакованном виде
     * 
     * @var string
     */
    protected $in_addr;
    
    /**
     * Адрес в развернутом виде
     * 
     * @var string
     */
    protected $expanded;
    
    public function __construct($ip)
    {
        if (!self::isValid($ip)) {
            throw new Exception("'$ip' не является валидным IPv6-адресом");
        }
        
        $this->value = $ip;
    }
    
    /**
     * Проверяет, является ли $ip валидным IPv6-адресом
     *
     * @param string $ip
     * @return boolean
     */
    public static function isValid($ip)
    {
        return (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) !== false);
    }
    
    /**
     * Возвращает IPv6-адрес в развернутом виде
     * 
     * return string
     */
    public function getExpanded()
    {
        if (!$this->expanded) {
            $hextets = array_filter(explode(':', $this->value), 'strlen');
            $zeroes = str_repeat(':0', 8 - count($hextets)) . ':';
            $ip = trim(str_replace('::', $zeroes, $this->value), ':');
        
            $hextets = explode(':', $ip);
            array_walk($hextets, function(&$el){
                $el = sprintf('%04s', $el);
            });
        
            $this->expanded = implode(':', $hextets);
        }
        
        return $this->expanded;
    }
}
