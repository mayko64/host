<?php
namespace Net;

abstract class Ip extends Payload
{
    /**
     * Преобразовывает упакованное значение в IP-адрес
     *
     * @param int $in_addr
     * @return string
     */
    public static function ntop($in_addr)
    {
        $ip = inet_ntop($in_addr);
    
        if (!$ip) {
            throw new Exception("Невозможно преобразовать '$in_addr' в IP-адрес");
        }
    
        return $ip;
    }
    
    /**
     * Упаковывает IP-адрес
     * 
     * @param string $ip
     * @return string
     */
    public static function pton($ip)
    {
        $in_addr = inet_pton($ip);
        
        if (!$in_addr) {
            throw new Exception("Невозможно упаковать адрес '$ip'");
        }
        
        return $in_addr;
    }
    
    /**
     * Возвращает адрес в упакованном виде
     *
     * @return string
     */
    public function getPacked()
    {
        if (!$this->in_addr) {
            $this->in_addr = self::pton($this->ip);
        }
    
        return $this->in_addr;
    }
}
