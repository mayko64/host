<?php
namespace Net;

/**
 * Аргумент запроса
 * 
 * От этого класса наследуются IP и Domain
 */
abstract class Payload
{
    /**
     * Текстовое представление аргумента (IP, домен)
     * 
     * @var string
     */
    public $value;
    
    /**
     * Проверяет, валиден ли $arg
     *
     * @param string $arg
     * @return boolean
     */
    public static function isValid($arg) {}
}
