<?php

use Net\Ipv4;
use Net\Ipv6;

function usage() {
    echo "Usage: ".basename(__FILE__)." <IP/hostname>", PHP_EOL;
}

if ($argc == 1) {
    usage();
    die();
}

if (version_compare(PHP_VERSION, '5.3.0') < 0) {
    die("Необходим PHP >= 5.3");
}

require __DIR__.'/include/autoload.php';

// сформируем список серверов
$transports = array();
$resolv_conf = file('/etc/resolv.conf');

foreach ($resolv_conf as $line) {
    $line = trim($line);
    
    if ($line[0] == ';' or $line[0] == '#')
        continue;
    
    list ($key, $host) = preg_split('/\s+/', $line);
    
    if ($key == 'nameserver' and (Ipv4::isValid($host) or Ipv6::isValid($host))) {
        $transport_type = 'socket';
        $transport = Transport\Factory::create($transport_type);
        $transport->setHost($host);
        $transports[] = $transport;
    }
}

$client = new Dns\Client();
$client->addTransports($transports);
$responses = $client->query($argv[1]);

foreach ($responses as $response) {
    echo (string)$response->answer;
}
